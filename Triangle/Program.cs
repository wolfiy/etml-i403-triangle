﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle
{
    internal class Program
    {
        private static int input;

        static void Main(string[] args)
        {
            do
            {
                // Nombre à utiliser et triangle.
                String triangle = "";
                String cross = "";
                String invertedTriangle = "";

                // Récupérer l'input tant qu'elle n'est pas valide.
                do
                {
                    Console.Clear();
                    Console.Write("Choisissez un nombre entre supérieur à 1: ");
                    input = Convert.ToInt16(Console.ReadLine());
                }
                while (!InputIsValid(input));

                cross = DrawCross(input);
                triangle = DrawTriangle(input);
                invertedTriangle = DrawInvertedTriangle(input);

                // Affichage du triangle.
                Console.WriteLine(triangle);
                Console.WriteLine(cross);
                Console.WriteLine(invertedTriangle);
                Console.WriteLine("[q] Quitter, [enter] réessayer");
            }
            while (Console.ReadKey().Key != ConsoleKey.Q);
        }

        private static String DrawCross(int input)
        {
            String _cross = "";

            for (int i = 0; i < input; ++i)
            {
                for (int j = 0; j < input; ++j)
                {
                    if (i == j || input - j - 1 == i) _cross += "X";
                    else _cross += " ";
                }
                _cross += "\n";
            }
            
            return _cross;
        }

        private static String DrawTriangle(int input)
        {
            String _triangle = "";
            for (int i = 0; i < input; ++i)
            {
                for (int j = 0; j <= i; ++j)
                {
                    if (input > 9) _triangle += "X";
                    else _triangle += Convert.ToString(input);
                }
                _triangle += "\n";
            }

            return _triangle;
        }

        private static String DrawInvertedTriangleAlt(int input)
        {
            String _triangle = "";
            for (int i = input - 1; i >= 0; --i)
            {
                for (int j = 0; j <= i; ++j)
                {
                    if (input > 9) _triangle += "X";
                    else _triangle += Convert.ToString(input);
                }
                _triangle += "\n";
            }

            return _triangle;
        }

        private static String DrawInvertedTriangle(int input)
        {
            String _triangle = "";
            for (int i = 0; i < input; ++i)
            {
                for (int j = 0; j < (input - i); ++j)
                {
                    if (input > 9) _triangle += "X";
                    else _triangle += Convert.ToString(input);
                }
                _triangle += "\n";
            }

            return _triangle;
        }

        private static Boolean InputIsValid(int input)
        {
            return input > 1;
        }
    }
}
